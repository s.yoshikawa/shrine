# coding: utf-8
from setuptools import setup, find_packages

setup(
    name="shrine",
    version="0.0.3",
    author="S.YOSHIKAWA",
    author_email="satoshi.yoshikawa1@persol.co.jp",
    packages=find_packages(),
    install_requires=[
        "jinjasql",
        "queries",
    ],
)
