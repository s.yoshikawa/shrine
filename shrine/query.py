# coding: utf-8
"""
SQLテンプレートファイル管理・実行クラス
:author S.YOSHIKAWA
:date   2019-10-23
"""
import jinjasql
import queries
import glob
import os.path
from typing import Any


# ==============================================================================
class Query(object):
    """
    テンプレートSQL読み込み、実行クラス
    """
    # --------------------------------------------------------------------------
    def __init__(self, uri: str, path_pattern: str = ""):
        """
        コンストラクタ
        :param uri: DB接続文字列
        :param path_pattern: SQLファイルpath文字列パターン
        """
        self.uri = uri
        self.sql = dict()

        if path_pattern:
            self.load_query(path_pattern=path_pattern)

    # --------------------------------------------------------------------------
    def load_query(self, path_pattern: str) -> None:
        """
        SQLテンプレートファイル読み込み
        :param path_pattern: SQLファイルpath文字列パターン
        :return: None
        """
        for element in glob.glob(path_pattern):
            with open(element, encoding="utf-8") as file:
                name, _ = os.path.splitext(os.path.basename(element))
                self.sql.setdefault(name, file.read())

    # --------------------------------------------------------------------------
    def exec(self, _name_: str, **kwargs) -> Any:
        """
        SQLテンプレートファイル実行
        :param _name_: テンプレート名(拡張子なしファイル名)
        :param kwargs: バインドパラメータ
        :return: 実行結果(SQL内容に依存)
        """
        q, p = jinjasql.JinjaSql(param_style="format").prepare_query(self.sql[_name_], kwargs)
        with queries.Session(uri=self.uri) as session:
            return [dict(row) for row in session.query(q, tuple(p)) or []]
